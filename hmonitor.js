const fs = require('fs');
const os = require('os');
const filepath = process.platform === 'linux' ? '/etc/hosts' : 'C:\\Windows\\System32\\drivers\\etc\\hosts';
const readline = require('readline');

console.log(filepath)

let _t = null;
let iteration = 0;

class HostEntry {
	constructor(address, domain){
		this.address = address;
		this.domain = domain;	
		this.monit = true;
	}

	setAddress(ip){
		this.address = ip;

		return this;
	}

	setMonit(monit = false){
		this.monit = monit;

		return this;
	}

	toLine(){
		return `${this.address} ${this.domain}${this.monit ? ' # [MONIT]' : ''}`;
	}
}

async function processLineUntil(findIt = null) { 
  const fileStream = fs.createReadStream(filepath);
  const entries = [];

  const source = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  }); 

  for await (const line of source) {  

  	let hasMonit;

    const lineData = line.split(' ');

    const condition = findIt !== null ? line.indexOf(findIt) > -1 : line.indexOf('[MONIT]') > -1; 

    if(condition){
    	const it = lineData[0].trim() === '#' ? 0 : -1;
    	const entry = new HostEntry(lineData[it+1], lineData[it+2]);
    	entries.push(entry);

	    if(findIt !== null){
	    	return entry;
	    }
	}
  }

  return entries;
}

const replaceLine = (line, dockerEntries) => {
	var formatted = line;	  	
	for (const dEntry of dockerEntries) {		
		if(line.indexOf(' '+dEntry.domain) > -1){		
			// console.log(dEntry);
			return line.replace(line, dEntry.toLine());
		}
	}

	return line;
}

const run = async () => {
	const dockerIp = await processLineUntil('host.docker.internal');
	const gatewayEntry = new HostEntry(dockerIp.address, 'gateway.docker.internal');

	const dockerEntries = await processLineUntil();
	dockerEntries.push(dockerIp.setMonit(false));
	dockerEntries.push(gatewayEntry.setMonit(false));


	for await (const dEntry of dockerEntries) {
		dEntry.setAddress(dockerIp.address);
	}


	const fileStream = fs.createReadStream(filepath);
	const entries = [];

	const source = readline.createInterface({
	input: fileStream,
	crlfDelay: Infinity
	});


	let fileLines = '';
	let old = '';

	for await (const line of source) {  
		old += line + os.EOL;
		fileLines += replaceLine(line, dockerEntries) + os.EOL;
	}

  fs.writeFile(filepath, fileLines, 'utf8', function (err) {
	    if (err) return process.stdout.write(err);		
	});

	iteration++;

	process.stdout.clearLine();
	process.stdout.cursorTo(0);
	process.stdout.write(`processed (running ${Math.floor((iteration*30)/60)}m): \n${fileLines}\r`);

}

run();

setInterval(() => {
	process.stdout.write('Interval executed.')
	run();
}, 30000);